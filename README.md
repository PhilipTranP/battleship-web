# The BattleShip Game [Play Here](https://battleship-game.netlify.com/)

> Never heard of the battleship game? [Read the battleship on wiki](<https://en.wikipedia.org/wiki/Battleship_(game)>).

# Install, test, and run (with `webpack-dev-server` installed)

```
npm install
npm run build
npm run serve
npm test
```

# The game is developed with the following guidelines:

### 1. Understand the problem

### 2. Do prior art research

> [This](http://en.battleship-game.org) is probably the top battleship game in term of features and playability. It even has a unique domain [play battleship-game.org](http://en.battleship-game.org)

> [Chris](http://christopherokhravi.com/) provides interesting perspective about making games using functional programing [watch his youtube video](https://www.youtube.com/watch?v=bRlvGoWz6Ig&t=2s)

### 3. Define business logic

This is probably the most important step. Business goals drive designs and set realistic milestones. This step is to ensure
every line of code written bringing in some real value ( and contributing to the 'making something that people want')

### 4. Create 'just a good enough version' to perform some user testings to validate some business logics/assumptions

For this type of project, test activities would be:

Test activity 1: Ads can be placed on the gif image display section without affecting user engagement.

Test activity 2: Having a scrolling log message area and make the screen longer will increase user engagement.

If null hypothesis of the test activity 2 (making the page longer makes no difference) is rejected then:

Possible test activity 3: Ads can be placed strategically (i.e., at the bottom) of the log area without impacting user engagement.

# Thank You!
