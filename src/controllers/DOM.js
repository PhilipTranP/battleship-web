import Events from "./Events"
import config from "../config"

let rotationEnabled = true

const addEventListener = function (battleShip) {
  let resetButton = document.getElementById("reset")
  resetButton.addEventListener("click", () => battleShip.newGame())
}

const renderBoard = (player, opponent, view) => {
  const gameboard = player.gameboard
  const boardElement = createElement("table")

  for (let y = 0; y < gameboard.size; y++) {
    const row = createElement("tr")
    boardElement.appendChild(row)

    for (let x = 0; x < gameboard.size; x++) {
      const cell = createElement("td", { id: `cell-${x}-${y}` })
      if (player.name === "cpu") {
        cell.addEventListener("click", () => opponent.attack(x, y, opponent))
      }
      row.appendChild(cell)
    }
  }
  view.innerHTML = ""
  view.appendChild(boardElement)

  Events.subscribe("turn", () => {
    rotationEnabled = false
  })
}

const renderShips = (player, view) => {
  const gameboard = player.gameboard

  const createShipElement = (cell, x, y) => {
    if (cell.ship !== undefined && cell.index === 0) {
      const ship = cell.ship
      const cellElement = view.querySelector(`#cell-${x}-${y}`)
      let width, height
      if (ship.direction === "h") {
        width = `calc(${100 * cell.ship.length}% + ${cell.ship.length}px)`
        height = "100%"
      } else if (ship.direction === "v") {
        width = "100%"
        height = `calc(${100 * cell.ship.length}% + ${cell.ship.length}px)`
      }
      const shipElement = createElement("div", {
        className: "ship",
        "style.height": height,
        "style.width": width
      })
      shipElement.addEventListener("click", event => {
        const [, x, y] = event.target.parentNode.id
          .split("-")
          .map(item => Number(item))
        if (rotationEnabled && gameboard.rotate(x, y)) {
          const newWidth = event.target.style.height
          event.target.style.height = event.target.style.width
          event.target.style.width = newWidth
        }
      })
      cellElement.appendChild(shipElement)
    }
  }
  gameboard.iterate(createShipElement)
}

const renderPegs = (gameboard, view) => {
  let attackState = gameboard.attackState

  for (let x = 0; x < attackState.length; x++) {
    for (let y = 0; y < attackState[x].length; y++) {
      let status = attackState[x][y]
      if (
        status === config.attackState.hit ||
        status === config.attackState.missed
      ) {
        let pegElement = createElement("div", {
          className: status === config.attackState.hit ? "peg hit" : "peg miss"
        })
        view.querySelector(`#cell-${x}-${y}`).append(pegElement)
      }
    }
  }
}

const updateFeed = (message, feedQueue) => {
  const messageElement = createElement("p", {
    textContent: message
  })
  feedQueue.push(messageElement)
}

const renderFeed = feedQueue => {
  const feed = document.getElementById("feed")
  while (feed.firstChild) feed.removeChild(feed.firstChild)

  feedQueue.forEach(messageElement => {
    feed.prepend(messageElement)
  })
}

const createElement = (tag, properties) => {
  const element = document.createElement(tag)

  // Sets nested properties from string
  for (const item in properties) {
    const split = item.split(".")

    let property = element
    let i
    for (i = 0; i < split.length - 1; i++) {
      property = property[split[i]]
    }
    property[split[i]] = properties[item]
  }
  return element
}

const DOM = {
  renderBoard,
  renderShips,
  updateFeed,
  renderPegs,
  renderFeed,
  addEventListener
}

export default DOM
