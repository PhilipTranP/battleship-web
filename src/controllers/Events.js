import DOM from "./DOM"

class Events {
  constructor () {
    this.events = {}
    this.sunk = false
  }

  subscribe (eventName, call) {
    if (typeof call !== "function") {
      console.error({
        eventName: eventName,
        call: call
      })
      throw Error("callback is not a function")
    }
    if (this.events[eventName]) {
      this.events[eventName].push(call)
    } else {
      this.events[eventName] = [call]
    }
  }

  publish (eventName, data) {
    if (this.events[eventName]) {
      this.events[eventName].forEach(call => {
        call(data)
      })
    } else {
      console.error(`event ${eventName} does not exist`)
    }
  }
}

export default new Events()
