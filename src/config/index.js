const config = {
  boardSize: 10,
  attackState: {
    none: 0,
    hit: 1,
    missed: 2,
    attacked: 3
  },
  gameState: {
    unready: 0,
    playing: 1,
    ended: 2
  }
}

export default config
