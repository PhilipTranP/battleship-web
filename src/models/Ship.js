import Events from "../controllers/Events"

class Ship {
  constructor (length, name) {
    if (!length) {
      throw Error("ship requires length")
    }

    this.length = length
    this.name = name
    this.hits = new Array(length).fill(0)
  }

  hit (index) {
    if (index >= 0 && index < this.length) {
      this.hits[index] = 1
      if (this.isSunk()) {
        Events.publish("sunk")
      }
    } else if (process.env.NODE_ENV !== "test") {
      console.error("hit index out of ship range")
    }
  }

  isSunk () {
    const totalHits = this.hits.reduce((total, x) => total + x)
    if (totalHits === this.length) {
      return true
    } else {
      return false
    }
  }
}

const Fleet = () => {
  return [
    new Ship(5, "carrier"),
    new Ship(4, "battleship"),
    new Ship(3, "cruiser"),
    new Ship(2, "destroyer"),
    new Ship(2, "destroyer"),
    new Ship(1, "submarine"),
    new Ship(1, "submarine")
  ]
}

export { Ship, Fleet }
