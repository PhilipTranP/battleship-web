import GameBoard from "./GameBoard"
import { Fleet } from "./Ship"
import DOM from "../controllers/DOM"
import Events from "../controllers/Events"
import config from "../config"
import moment from "moment"

class BattleShip {
  constructor () {
    this.playerView = undefined
    this.opponentView = undefined
    this.boardOne = undefined
    this.boardTwo = undefined
    this.player = undefined
    this.computer = undefined
    this.gameState = config.gameState.unready

    const feedQueue = []
  }

  init () {
    DOM.addEventListener(this)
    this.subscribeEvents()

    this.playerView = document.getElementById("player")
    this.opponentView = document.getElementById("opponent")

    this.boardOne = new GameBoard(config.boardSize)
    this.boardTwo = new GameBoard(config.boardSize)

    this.player = this.boardOne.createPlayer(
      "player",
      this.boardOne,
      this.boardTwo
    )
    this.computer = this.boardTwo.createPlayer(
      "cpu",
      this.boardTwo,
      this.boardOne
    )
  }

  setGameState (newState) {
    this.gameState = newState
    Events.publish("gameStateChanged", { gameState: this.gameState })
  }

  subscribeEvents () {
    Events.subscribe("turn", (data = {}) => {
      DOM.renderPegs(this.boardOne, this.playerView)
      DOM.renderPegs(this.boardTwo, this.opponentView)
      DOM.renderFeed(this.feedQueue)

      if (this.player.won()) {
        Events.publish("victory")
      } else if (this.player.lost()) {
        Events.publish("loss")
      }

      if (data.attackState !== config.attackState.attacked) {
        this.computer.randomAttack()

        DOM.renderPegs(this.boardOne, this.playerView)
        DOM.renderFeed(this.feedQueue)
      }
    })

    Events.subscribe("playerHit", data => {
      const timeStamp = moment().format("LTS")
      const currentImage = document.getElementById("brand")
      if (Events.sunk === true) {
        DOM.updateFeed(
          `${timeStamp} we shot at |>${data.x}, ${data.y}<| and sunk one!`,
          this.feedQueue
        )
        currentImage.src = "../assets/sunk.gif"
        setTimeout(() => {
          currentImage.src = "../assets/brand.jpeg"
        }, 2000)
      } else {
        DOM.updateFeed(
          `${timeStamp} we shot at |>${data.x}, ${data.y}<| and hit 'em!`,
          this.feedQueue
        )
        currentImage.src = "../assets/hit.gif"
      }
      Events.sunk = false
    })

    Events.subscribe("playerMiss", data => {
      const timeStamp = moment().format("LTS")
      DOM.updateFeed(
        `${timeStamp} we shot at |> ${data.x}, ${data.y} <| and missed.`,
        this.feedQueue
      )
    })

    Events.subscribe("cpuHit", data => {
      const timeStamp = moment().format("LTS")
      document.getElementById("brand").classList.add("shake")
      setTimeout(
        () => document.getElementById("brand").classList.remove("shake"),
        100
      )
      if (Events.sunk === true) {
        DOM.updateFeed(
          `${timeStamp} we lost a ship at |> ${data.x}, ${data.y} <|`,
          this.feedQueue
        )
      } else {
        DOM.updateFeed(
          `${timeStamp} they shot us at |> ${data.x}, ${
            data.y
          }|> and hit our ship`,
          this.feedQueue
        )
      }
      Events.sunk = false
    })

    Events.subscribe("cpuMiss", data => {
      const timeStamp = moment().format("LTS")
      DOM.updateFeed(
        `${timeStamp} they shot us at |> ${data.x}, ${data.y}|> and missed!`,
        this.feedQueue
      )
    })

    Events.subscribe("victory", data => {
      const timeStamp = moment().format("LTS")
      DOM.updateFeed(
        `${timeStamp} we've sunk their entire fleet!`,
        this.feedQueue
      )
      DOM.updateFeed("WE <> W I N <> THE <> BATTLE", this.feedQueue)
      this.setGameState(config.gameState.ended)
    })

    Events.subscribe("loss", data => {
      const timeStamp = moment().format("LTS")
      DOM.updateFeed(`${timeStamp} we lost our fleet!`, this.feedQueue)
      DOM.updateFeed("THE <> GAME <> IS <> O V E R", this.feedQueue)
      this.setGameState(config.gameState.ended)
    })

    Events.subscribe("sunk", () => {
      Events.sunk = true
    })

    Events.subscribe("started", () => {
      DOM.updateFeed(
        "|> use NEW GAME button to select ideal positions <|",
        this.feedQueue
      )
      DOM.renderFeed(this.feedQueue)
      setTimeout(() => {
        DOM.updateFeed(
          "|> When you're ready, just start firing! <|",
          this.feedQueue
        )
        DOM.renderFeed(this.feedQueue)
      }, 500)
    })
  }

  reset () {
    this.setGameState(config.gameState.unready)

    this.feedQueue = []
    DOM.renderFeed(this.feedQueue)
  }

  newGame () {
    this.reset()

    this.boardOne.clean()
    this.boardTwo.clean()

    this.boardOne.placeRandom(Fleet())
    this.boardTwo.placeRandom(Fleet())

    DOM.renderBoard(this.player, this.computer, this.playerView)
    DOM.renderBoard(this.computer, this.player, this.opponentView)
    DOM.renderShips(this.player, this.playerView)

    this.setGameState(config.gameState.playing)
    Events.publish("started")
  }
}

export default new BattleShip()
