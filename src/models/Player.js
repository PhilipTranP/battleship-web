import Events from "../controllers/Events"
import config from "../config"

class Player {
  constructor (name, gameboard, antiBoard) {
    if (!name || !gameboard || !antiBoard) {
      throw Error("player missing arguments")
    }

    this.name = name
    this.gameboard = gameboard
    this.antiBoard = antiBoard
  }

  attack (x, y) {
    if (this.antiBoard.onBoard(x, y) && this.antiBoard.isPlaying()) {
      const result = this.antiBoard.receiveAttack(x, y)
      if (result === config.attackState.hit) {
        Events.publish("playerHit", { x: x, y: y })
        Events.publish("turn")
      } else if (result === config.attackState.missed) {
        Events.publish("playerMiss", { x: x, y: y })
        Events.publish("turn")
      } else {
        Events.publish("turn", { attackState: result })
      }
      return true
    } else {
      return false
    }
  }

  randomAttack () {
    while (true) {
      let xy = [
        Math.floor(Math.random() * this.gameboard.size),
        Math.floor(Math.random() * this.gameboard.size)
      ]

      if (!this.antiBoard.attackState[xy[0]][xy[1]]) {
        if (this.antiBoard.receiveAttack(xy[0], xy[1])) {
          Events.publish("cpuHit", { x: xy[0], y: xy[1] })
        } else {
          Events.publish("cpuMiss", { x: xy[0], y: xy[1] })
        }
        break
      }
    }
  }

  won () {
    return this.antiBoard.allSunk()
  }

  lost () {
    return this.gameboard.allSunk()
  }
}

export default Player
