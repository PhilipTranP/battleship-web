import Player from "./Player"
import config from "../config"
import Events from "../controllers/Events"

class GameBoard {
  constructor (size) {
    if (!size || size < 10) {
      throw Error("Gameboard size must greater or equals 10")
    }

    this.size = size
    this.board = []
    this.attackState = []
    this.ships = []
    this.opponent = undefined
    this.gameState = config.gameState.unready

    this.reset()
    this.registerEvent()
  }

  registerEvent () {
    Events.subscribe(
      "gameStateChanged",
      ({ gameState = config.gameState.unready } = {}) => {
        this.gameState = gameState
      }
    )
  }

  clean () {
    this.reset()
  }

  reset () {
    this.ships = []
    this.board = []
    this.attackState = []
    this.opponent = undefined

    for (let i = 0; i < this.size; i++) {
      this.board[i] = new Array(this.size).fill(0)
      this.attackState[i] = new Array(this.size).fill(0)
    }
  }

  isPlaying () {
    return this.gameState == config.gameState.playing
  }

  createPlayer (name, gameboard, antiBoard) {
    let player = new Player(name, gameboard, antiBoard)
    this.opponent = player
    return player
  }

  randomCoordinate (min = 0, max = this.size - 1) {
    return Math.floor(Math.random() * (max - min + 1)) + min
  }

  onBoard (x, y) {
    return !!(this.board[x] !== undefined && this.board[x][y] !== undefined)
  }

  setSquare (x, y, value) {
    if (this.onBoard(x, y)) {
      this.board[x][y] = value
    } else {
      console.error("no such square", [x, y])
    }
  }

  cell (x, y) {
    // if (0) evaluates to false!
    if (this.onBoard(x, y)) {
      return this.board[x][y]
    } else {
      console.error("no such square", [x, y])
    }
  }

  iterate (cellCallback = () => {}, rowCallback = () => {}) {
    for (let y = 0; y < this.size; y++) {
      rowCallback(this.board[y], y)
      for (let x = 1; x < this.size; x++) {
        cellCallback(this.board[x][y], x, y)
      }
    }
  }

  available (ship, x, y) {
    if (this.cell(x, y) === undefined) {
      return false
    } else if (this.cell(x, y) === 0 || this.cell(x, y).ship === ship) {
      return true
    } else {
      return false
    }
  }

  gatherSpaces (ship, x, y, direction) {
    const pairs = []
    for (let i = 0; i < ship.length; i++) {
      if (direction === "h") {
        if (!this.available(ship, x + i, y)) {
          return false
        }
        pairs.push({ x: x + i, y: y })
      } else if (direction === "v") {
        if (!this.available(ship, x, y + i)) {
          return false
        }
        pairs.push({ x: x, y: y + i })
      } else {
        console.error("invalid direction")
        return false
      }
    }
    return pairs
  }

  place (ship, x, y, direction) {
    const spaces = this.gatherSpaces(ship, x, y, direction)

    if (spaces === false) {
      return false
    }

    spaces.forEach((pair, index) => {
      this.setSquare(pair.x, pair.y, { ship: ship, index: index })
    })

    Object.assign(ship, { x: x, y: y, direction: direction })
    this.ships.push(ship)
    return true
  }

  remove (ship) {
    for (let i = 0; i < ship.length; i++) {
      if (ship.direction === "h") {
        this.setSquare(ship.x + i, ship.y, 0)
      } else if (ship.direction === "v") {
        this.setSquare(ship.x, ship.y + i, 0)
      }
    }
    this.ships.slice(this.ships.indexOf(ship))
  }

  move (x0, y0, x1, y1) {
    const ship = this.cell(x0, y0).ship
    if (!ship) {
      throw Error("no ship to move")
    }

    if (this.gatherSpaces(ship, x1, y1, ship.direction) !== false) {
      this.remove(ship)
      return this.place(ship, x1, y1, ship.direction)
    } else {
      return false
    }
  }

  rotate (x, y) {
    const ship = this.cell(x, y).ship

    if (!ship) {
      console.error([x, y])
      throw Error("no ship to rotate")
    }

    const newDirection = ship.direction === "h" ? "v" : "h"

    if (this.gatherSpaces(ship, x, y, newDirection) !== false) {
      this.remove(ship)
      return this.place(ship, x, y, newDirection)
    } else {
      return false
    }
  }

  placeRandom (ships) {
    ships.forEach(ship => {
      const direction = Math.random() < 0.5 ? "h" : "v"
      let x = 0
      let y = 0
      let count = 0
      do {
        if (direction === "h") {
          x = this.randomCoordinate(0, this.size - 1 - ship.length)

          y = this.randomCoordinate()
        } else if (direction === "v") {
          x = this.randomCoordinate()
          y = this.randomCoordinate(2, this.size - 1 - ship.length)
        }
      } while (count++ < 100 && this.place(ship, x, y, direction) === false)
    })
  }

  receiveAttack (x, y) {
    if (this.attackState[x][y]) {
      return config.attackState.attacked
    }

    const square = this.cell(x, y)
    if (square === 0) {
      this.attackState[x][y] = config.attackState.missed
      return config.attackState.missed
    } else if (square.ship) {
      square.ship.hit(square.index)
      this.attackState[x][y] = config.attackState.hit
      return config.attackState.hit
    }
  }

  allSunk () {
    let allSunk = true
    this.ships.forEach(ship => {
      if (!ship.isSunk()) {
        allSunk = false
      }
    })

    return allSunk
  }
}

export default GameBoard
