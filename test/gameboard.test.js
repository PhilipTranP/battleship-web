import Gameboard from "../src/models/GameBoard"
import { Ship } from "../src/models/Ship"

jest.mock("../src/controllers/Events")

test("has size", () => {
  const gameboard = new Gameboard(10)
  expect(gameboard.size).toEqual(10)
})

test("must have size", () => {
  expect(() => new Gameboard()).toThrow()
})

test("has sized greater than 9", () => {
  expect(() => new Gameboard(9)).toThrow()
})

test("sets board square", () => {
  const gameboard = new Gameboard(10)
  gameboard.setSquare(2, 2, 42)

  expect(gameboard.cell(2, 2)).toEqual(42)
})

test("iterates over board", () => {
  const gameboard = new Gameboard(10)
  const result = []
  const cellCallback = (cell, x) => {
    result.push(x)
  }
  const rowCallback = (row, y) => {
    result.push(y)
  }
  gameboard.iterate(cellCallback, rowCallback)
  expect(result).toHaveLength(100)
})

test("places ship horizontally", () => {
  const ship = { length: 3 }
  const gameboard = new Gameboard(10)

  gameboard.place(ship, 0, 0, "h")
  expect(gameboard.cell(0, 0)).toMatchObject({ ship: ship, index: 0 })
  expect(gameboard.cell(1, 0)).toMatchObject({ ship: ship, index: 1 })
  expect(gameboard.cell(2, 0)).toMatchObject({ ship: ship, index: 2 })
  expect(ship).toMatchObject({ x: 0, y: 0, direction: "h" })
})

test("places ship vertically", () => {
  const ship = { length: 3 }
  const gameboard = new Gameboard(10)

  gameboard.place(ship, 0, 0, "v")
  expect(gameboard.cell(0, 0)).toMatchObject({ ship: ship, index: 0 })
  expect(gameboard.cell(0, 1)).toMatchObject({ ship: ship, index: 1 })
  expect(gameboard.cell(0, 2)).toMatchObject({ ship: ship, index: 2 })
  expect(ship).toMatchObject({ x: 0, y: 0, direction: "v" })
})

test("moves ship", () => {
  const ship = { length: 3 }
  const gameboard = new Gameboard(10)

  gameboard.place(ship, 0, 2, "h")
  gameboard.move(0, 2, 1, 2)
  expect(gameboard.cell(1, 2)).toMatchObject({ ship: ship, index: 0 })
})

test("rotates ship", () => {
  const ship = { length: 3 }
  const gameboard = new Gameboard(10)

  gameboard.place(ship, 0, 0, "h")
  expect(gameboard.rotate(0, 0)).toEqual(true)
  expect(gameboard.cell(0, 2)).toMatchObject({ ship: ship, index: 2 })
})

test("returns attack outcome", () => {
  const ship = new Ship(3)
  const gameboard = new Gameboard(10)
  gameboard.place(ship, 0, 0, "h")

  expect(gameboard.receiveAttack(1, 0)).toEqual(1)
  expect(gameboard.receiveAttack(2, 2)).toEqual(2)
})

test("sends attack to ship", () => {
  const ship = new Ship(3)
  const gameboard = new Gameboard(10)
  gameboard.place(ship, 0, 0, "h")

  gameboard.receiveAttack(1, 0)
  expect(ship.hits).toEqual([0, 1, 0])
})

test("reports all ships sunk", () => {
  const smallShip = { length: 3, isSunk: () => true }
  const largeShip = { length: 5, isSunk: () => false }
  const gameboard = new Gameboard(10)
  gameboard.place(smallShip, 0, 1, "v")
  gameboard.place(largeShip, 0, 0, "h")

  expect(gameboard.allSunk()).toEqual(false)

  largeShip.isSunk = () => true
  expect(gameboard.allSunk()).toEqual(true)
})
