import Player from "../src/models/Player"
import { Ship } from "../src/models/Ship"
import Gameboard from "../src/models/Gameboard"

jest.mock("../src/controllers/Events")

test("has name and board", () => {
  const player = new Player("Marvin", {}, {})

  expect(() => Player()).toThrow()
  expect(player.name).toEqual("Marvin")
})

test("unsuccessful move returns false", () => {
  const antiBoard = new Gameboard(10)
  const player = new Player("x", {}, antiBoard)

  expect(player.attack(11, 11)).toEqual(false)
})

test("registers victory", () => {
  const antiBoard = { allSunk: () => true }
  const player = new Player("x", {}, antiBoard)

  expect(player.won()).toEqual(true)
})

test("registers loss", () => {
  const board = { allSunk: () => true }
  const player = new Player("x", board, {})

  expect(player.lost()).toEqual(true)
})
